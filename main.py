# -*- coding: utf-8 -*-
# Module: default
# Author: Vladimir Milosevic
# Created on: 23.11.2016.
# License: GPL v.3 https://www.gnu.org/copyleft/gpl.html

import sys
from urlparse import parse_qsl
import xbmc, xbmcgui, xbmcplugin, xbmcaddon
import prva

# Get the plugin url in plugin:// notation.
_url = sys.argv[0]
# Get the plugin handle as an integer number.
_handle = int(sys.argv[1])


prva.set_path(xbmc.translatePath(xbmcaddon.Addon().getAddonInfo('profile')).decode('utf-8'))


def create_items(items, action, folder):
		"""
		Create Kodi folder items
		"""
		listing = []
		for key, item in items.iteritems():

			list_item = xbmcgui.ListItem(label=item['name'])
			list_item.setArt({'thumb': 	item['thumb'],
												'icon': 	item['thumb'],
												'fanart':	item['thumb']})

			list_item.setInfo('video', {'title': item['name'], 'genre': item['name']})
			url = '{0}?action={1}&id={2}&uri={3}'.format(_url, action, item['id'], item['uri'])
			
			if not folder:
				list_item.setProperty('IsPlayable', 'true')
			
			listing.append((url, list_item, folder))

		xbmcplugin.addDirectoryItems(_handle, listing, len(listing))
		xbmcplugin.addSortMethod(_handle, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
		xbmcplugin.endOfDirectory(_handle)


def play_video(path):
		"""
		Play a video by the provided path.
		"""
		play_item = xbmcgui.ListItem(path=path)
		xbmcplugin.setResolvedUrl(_handle, True, listitem=play_item)


def router(paramstring):
		"""
		Router function that calls other functions
		depending on the provided paramstring
		"""
		# Parse a URL-encoded paramstring to the dictionary of
		# {<parameter>: <value>} elements
		params = dict(parse_qsl(paramstring))
		# Check the parameters passed to the plugin
		if params:
				if params['action'] == 'list-episodes':
						items = prva.get_episodes(params['id'], params['uri'])
						create_items(items, 'list-parts', True)

				if params['action'] == 'list-parts':
						items = prva.get_parts(params['id'], params['uri'])
						create_items(items, 'play', False)

				elif params['action'] == 'play':
						play_video(params['uri'])

		else:
				items = prva.get_shows()
				create_items(items, 'list-episodes', True)


if __name__ == '__main__':
		# Call the router function and pass the plugin call parameters to it.
		# We use string slicing to trim the leading '?' from the plugin call paramstring
		router(sys.argv[2][1:])