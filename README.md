# kodi-prva-rs
Kodi video addon that lists program and tv shows from prva.rs.

# Installation

Download repo as zip file and save it somewhere convenient. From Kodi
go to System->Add-ons->Install from zip file and find downloaded file.

After installation is done, you can find plugin from Kodi home screen
Videos->Add-ons->Prva.Rs.