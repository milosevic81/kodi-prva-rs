from bs4 import BeautifulSoup
import urllib
import json
import os.path, time, re

"""
Scrap www.prva.rs for tv-shows using BeautifulSoup. Get all shows, episodes
and episode parts so they can be opend in Kodi.
List of shows and episodes is cached.
"""

markup = 'html.parser'
site = 'http://www.prva.rs'

## private

def read_file(file):
	with open(file, 'r') as f:
		try:
			return json.load(f)
		except ValueError:
			return {}

def save_file(file, dict):
	with open(file, 'w') as f:
		json.dump(dict, f)

def is_old(file):
	try:
		fileCreation = os.path.getctime(file)
		ago = time.time() - 60*60*24*2
		return fileCreation < ago
	except:
		return True

def scan_all_shows():
	shows = {}
	categories = scan_categories()
	for key, category in categories.iteritems():
		scan_shows(category['uri'], shows)
	return shows

def scan_categories():
	categories = {}
	page = urllib.urlopen('http://www.prva.rs/web-tv.html').read()
	soup = BeautifulSoup(page, markup)
	for category in soup.find(id='topFull').div.ul.find_all('li'):
		uri = site + category.a['href']
		name = category.a.span.get_text().strip().encode('utf-8', 'replace')
		id = name
		categories[id] = {'id': id, 'name': name, 'uri': uri}
	return categories


def scan_shows(category_uri, shows):
	page = urllib.urlopen(category_uri).read()
	soup = BeautifulSoup(page, markup)
	for show in soup.find(id='heroChildren').div.div.find_all('div'):
		if show['class'] == ['children-box', 'hero-item', 'red']:
			name = show.h3.a.get_text().strip().encode('utf-8', 'replace')
			uri = site + show.h3.a['href']
			thumb = site + show.a.img['src']
			# use last part of uri (filename) as id
			id = uri.rsplit('/', 1)[-1].rstrip('.html')
			shows[id] = {'id': id, 'name': name, 'uri': uri, 'thumb': thumb}

def scan_all_episodes(show_uri):
	"""
	TODO This currently scans only first page. TODO Scan all older pages as well
	"""
	episodes = {}
	scan_episodes(show_uri, episodes)
	return episodes

def scan_episodes(show_uri, episodes):
	page = urllib.urlopen(show_uri).read()
	soup = BeautifulSoup(page, markup)
	# TODO This only scans 1st page. If we have more than one page scan all
	for episode in soup.find(id='heroMedia').div.div.find_all('div'):
		if episode.has_attr("class") and episode['class'] == ['hero-item', 'single-box', 'media-box', 'red']:
			name = episode.h3.a.get_text().strip().encode('utf-8', 'replace')
			uri = site + episode.h3.a['href']
			thumb = site + episode.a.img['src']
			# use last part of uri (filename) as id
			id = uri.rsplit('/', 1)[-1].rstrip('.html')
			episodes[id] = {'id': id, 'name': name, 'uri': uri, 'thumb': thumb}

def scan_all_parts(episode_uri):
	parts = {}
	page = urllib.urlopen(episode_uri).read()
	soup = BeautifulSoup(page, markup)
	# get 1. part from page
	parts_cnt = 1
	scan_part(soup, parts, parts_cnt)
	# get list of other parts
	all_parts = soup.find(class_='mediaListBlock')
	if all_parts is not None:
		other_parts = all_parts.find_all('a')[1:]
		# scan other parts as well
		for a in other_parts:
			part_page = urllib.urlopen(site + a['href']).read()
			part_soup = BeautifulSoup(part_page, markup)
			parts_cnt += 1
			scan_part(part_soup, parts, parts_cnt)
	return parts


def scan_part(soup, parts, part_id):
	div_player = soup.find(id='plejer')
	script = div_player.script.get_text()
	uri = re.search("src: '(.+?)'", script).groups()[0]
	thumb = site + div_player.div.img['src']
	name = str(part_id) + '. deo'
	id = name
	parts[id] = {'id': id, 'name': name, 'uri': uri, 'thumb': thumb}


## public


def set_path(path):
	global addon_path
	if not os.path.exists(path):
		os.makedirs(path)
	addon_path = path


def get_shows():
	"""
	Scan prva.rs for all tv-shows
	"""
	shows = {}
	shows_file = addon_path + 'all.shows.txt'
	if is_old(shows_file):
		shows = scan_all_shows()
		save_file(shows_file, shows)
	else:
		shows = read_file(shows_file)
	return shows


def get_episodes(show_id, show_uri):
	"""
	Get all episodes of show_id
	"""
	episodes = {}
	cache_file = addon_path + show_id + '.txt'
	if is_old(cache_file):
		episodes = scan_all_episodes(show_uri)
		save_file(cache_file, episodes)
	else:
		episodes = read_file(cache_file)
	return episodes


def get_parts(episode_id, episode_uri):
	"""
	Get all episode parts
	"""
	parts = {}
	cache_file = addon_path + episode_id + '.txt'
	if is_old(cache_file):
		parts = scan_all_parts(episode_uri)
		save_file(cache_file, parts)
	else:
		parts = read_file(cache_file)
	return parts
